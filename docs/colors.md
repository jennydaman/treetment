# colors

https://www.colors.lol/milk-white

- [#fd3c06](https://www.colorhexa.com/fd3c06) (light red)
- [#985e2b](https://www.colorhexa.com/985e2b) (light brown)

#### other

- [#982b2f](https://www.colorhexa.com/982b2f) (dark red)
