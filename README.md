# Treetment

Rapid procedural classification of a new symptom's urgency.

*Treetment* features:

1. "clinician portal" where the user can serialize medical advice as a decision/dependency tree.
2. "patient portal" where the user can evaluate the urgency of predictable symptoms related to their medical condition.

# Objective

*Treetment* is a general solution for how to serialize and interpret advice. The intended use case is where a patient who is currently undergoing treatment needs to make a quick and informed decision about forseen complications. The history of the patient is known, and how to react can be predetermined.

It does *not* make diagnosis. (With all of their money, not even IBM could achieve that).

# How It Works

A patient's possible state is described by the clinician in terms of numerical variables.

#### Example

- `DIAGNOSIS.NMO == 1.0` means the patient has been diagnosed with neuromyelitis optica.
- `DIAGNOSIS.NMO == 0.0` means the patient is healthy.

The clinician can arbitrarily decide what values mean. for example, you could denote `DIAGNOSIS.NMO == 0.5` to mean that an NMO diagnosis is suspected but not confirmed.

Next, the clinician proposes a series of questions to ask depending on variable values.

> if `DIAGNOSIS.NMO == 1`, then ask: "Do you notice any change in your vision?"

The patient would answer (applicable) questions on an app. Answers produce more variables, e.g. `VISION.LEFT.BLURRY=2.2`. Which questions get asked depends on the patient's medical history. Questions can lead to more questions when the result is applicable.

Variables and questions together model the predictable complications of a medical treatment plan. The app reconstructs a dependency graph based on the conditionals that prelude questions. This graph is used as a decision tree. The root/entrypoint is the patient's medical history.

Applicable questions are pushed to a stack of more questions to ask. Essentially, the app does a depth-first search on the graph. Hence, more important followup questions are asked sooner. It is guaranteed that new questions which are directly dependent on the current outcome will be asked immediately next. For example,

1. "Is your sight blurry?" User responds *"yes"* => set `VISION.BLURRY.question=1`
2. Followup question "Which eye is blurry?" with options *"Left"*, *"Right"*, or *"Both"* will be asked next.

There are two possible outcomes. If a concerning variable is set, e.g. `VISION.BLOCKED == 1.0`, then a terminal/leaf node will be activated. These will be actions to either "call/email your doctor" or "urgent care, notify ER".
Otherwise, the patient's responses are not of concern. The queue would exhaust and propagation ends.
It is also possible that the patient's symptom was not included in the model. In this case, the patient is simply instructed to call their doctor.

<details>
<summary>
Example Sketch
</summary>

Patient has NMO but no history of ON. Wakes up and sees a gray spot in the right eye. The app would notify the patient that this is an urgent situation that requires immediate treatment. It would send a message to the doctor about this situation and alert the patient to report to an ER or urgent care center ASAP.

![simple tree drawing](docs/sketch_diagram.svg)

1.  the app will first look at the patient history. NMO positive, no ON
2.  "Do you feel tingling or burning?" no. => stop asking related questions about neuritis
3.  "Is your vision blurry?" no
4.  "Is your vision partially blocked?" yes
5.  leaf node: "urgent care ER"

</details>

# Specification 1

Version 1 of the model specification.
The model consists of variables, questions, and metadata. The information can be stored in JSON, see [examples/nmo.json](https://gitlab.com/jennydaman/treetment/-/blob/master/examples/nmo.json).

## Meta

Details about this model.

- `date`: when the model was last updated. `Date.now()`
- `specification`: specification version. `1`
- `title`: title of model
- `author`: name of person who created the model
- `description`: description of the model

## Variables

Declare variables with names, help, and type.

### Nesting

If a key has a subkey `variables`, then that variable is nested. For example, `DIAGNOSIS` is a nested variable. It cannot be given a value, but you can assign a value to `DIAGNOSIS.NMO`.

### help

Denote the biological meaning of a variable.

```json
"RITUXIMAB_DOSE": {
  "help": "IV [mg/m^2]"
}
```

### type

`number` (integer or float) is the only real data type. This is default.

`"type": "date"` is also supported. It might be more useful to store the date of an incident rather than a binary classification. `DIAGNOSIS.NMO == 1581622200000` can mean that patient was diagnosed on 2020-02-13.

It might be tempting to use a string data type. Strings are not supported, discrete variables are preferred.
Instead of `ON_SIDE == "RIGHT"`, define two variables: `ON_SIDE_LEFT == 0`, and `ON_SIDE_RIGHT == 1`.

### common type

Repeatable collection of subkeys for common enumerations.
The nested variables can be copied under multiple other distinct variables.

<details>
<summary>
Example: body location of symptom
</summary>

```json
"SYMPTOMS": {
  "help": "Symptoms reported by patient",
  "common": {
    "location": {
      "LEFT_ARM": {
        "help": "left arm"
      },
      "RIGHT_ARM": {
        "help": "right arm"
      },
      "NECK": {
        "help": "neck"
      }
    }
  },
  "variables": {
    "TINGLING": {
      "help": "Tingling sensation",
      "type": "location"
    },
    "BURNING": {
      "help": "Burning sensation",
      "type": "location"
    },
    "PAIN": {
      "help": "Burning sensation",
      "type": "location"
    },
    "WEAKNESS": {
      "help": "Weakness",
      "type": "location"
    }
  }
}
```

</details>

<details>
<summary>
Example: start date, end date, and dosage of drug
</summary>

```json
"PILLS": {
  "help": "daily pills",
  "common": {
    "daily_pill": {
      "DOSE": {
        "help": "prescribed daily dosage [mg/day]"
      },
      "DATE": {
        "help": "duration of treatment",
        "variables": {
          "START": {
            "help": "Start date",
            "type": "date"
          },
          "END": {
            "help": "End date",
            "type": "date"
          }
        }
      }
    }
  },
  "variables": {
    "MYCOPHENOLATE": {
      "help": "mycophenolate",
      "type": "daily_pill"
    },
    "METHOTREXATE": {
      "help": "methotrexate",
      "type": "daily_pill"
    },
    "PREDNISONE": {
      "help": "prednisone",
      "type": "daily_pill"
    }
  }
}
```

</details>


## Questions

Questions are nodes of the dependency graph.
The patient will be asked a question if its *Conditional* is met.

### Nested Questions

Related questions can be grouped together in a list `questions` under a "dummy" question object. This "dummy" question should not provide [`answer`](#answer).

[`ifs`](#ifs) and [`keywords`](#keywords) are inherited from parent question to nested questions.

#### ifs

type: [*Conditional*](#Conditionals), see below

Usually, the top-level *Conditional* for a question will be an `["or" ...]` expression listing several different criteria for which it would be appropriate to ask the question.

#### Conditionals

data definition: a *Conditional* can take one of four forms:

- `true` (boolean) always ask this question.
- `1.3` (number) will be evaluated as ["truthy"](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) (tl;dr "not equal 0.0")
- `"DIAGNOSIS.NMO"` (string) will be substituted for the variable's numerical value
- `["=" "DIAGNOSIS.NMO", 3]` (array) a prefix boolean expression (similar to LISP), nested with following *Conditionals*

Operation  | Example | Description
-----------|---------|-------------
comparison |`["=", "NUMBER_OF_HANDS", 2]` | Standard JavaScript [comparison operators](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators): `"=="`, `"!="`, `"<"`, `">"`, `"<="`. `">="`. (example means patient has two hands)
**NOT**    |`["not", "DIAGNOSIS.NMO"]`      | Negation (example means patient is not diagnosed with neuromyelitis optica)
**AND**    |`["and", "DIAGNOSIS.NMO", "RADIOLOGY.0N"]` | True if all following arguments are true (example means patient is both diagnosed with neuromyelitis optica and has optic neuritis)
**OR**     |`["or", "DIAGNOSIS.NMO", "RADIOLOGY.0N"]`| True if at least one of the following arguments are true (example means patient is either diagnosed with neuromyelitis optica, has optic neuritis, or both)

### Title

type: string

The question to ask, or the name of this group of multiple questions

### Keywords

type: list of strings

List of keywords which lead to this question when searched.
The list should be automatically generated by the *clinician portal* from variable names and question text, and can also include custom entries.

### answer

type: object (optional)

If answer is not defined, then the question will be **fill-in-the-blank** with a numerical response.

`answer` should be key-value pairs, where the `key` is the text to display for an error (e.g. `"My vision is blurry"`) and the value needs to be a list of [*Updates*](#update). Empty list (`[]`) is a no-op.

#### update

Data definition: an *Update* is an array of length 3.

Index | Value
------|-------
`[0]` | prefix operation: one of `=`, `+`, or `*`
`[1]` | name of variable to update
`[2]` | argument

##### Examples

```javascript
["=", "VISION.BLURRY", 2.5]     // set VISION.BLURRY to 2.5
["+", "NUMBER_OF_SYMPTOMS", 1]  // add 1 to NUMBER_OF_SYMPTOMS
["+", "REASONS_TO_CRY", -1]     // subtract 1 from REASONS_TO_CRY
["*", "PAIN", 3]                // triple the value of PAIN
["*", "BUDGET", 0.25]           // divide BUDGET by 4
```

```json
{
  "title": "Which eye is blurry?",
  "ifs": [">", "VISION.BLURRY.question", 0.5],
  "keywords": ["eye", "vision", "blurry", "impaired"],
  "answer": {
    "Left": [["=", "VISION.BLURRY.LEFT", 1]],
    "Right": [["=", "VISION.BLURRY.RIGHT", 1]],
    "Both": [
      ["=", "VISION.BLURRY.LEFT", 1],
      ["=", "VISION.BLURRY.RIGHT", 1]
    ],
    "Neither": []
  }
}
```

